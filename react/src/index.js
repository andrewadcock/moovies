import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Link} from 'react-router-dom';
import Movies from "./components/Movies";
import Genres from "./components/Genres";


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <nav>
        <Link to='/'>Movies</Link>
        <Link to='/genres'>Genres</Link>
      </nav>
      <Route exact path='/' component={Movies} />
      <Route exact path='/genres' component={Genres} />
    </Router>

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
