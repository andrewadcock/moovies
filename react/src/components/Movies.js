import React, {Component} from 'react';
import axios from 'axios';

class Movies extends Component {

    constructor() {
        super();
        this.state = {
            movies: '',
        }
    }

    async componentDidMount() {
        const {data} = await axios.get('http://localhost:8000/movies');
        console.log("Data", data);

        this.setState({
            movies: data.movies
        })

        console.log('Type Of', typeof this.state.movies);
    }

    render() {
        return (
            <div className='container'>
                <h1 className='page-title'>Movies</h1>


        { /*
        this.state.movies.map( function(movie) {
            return "<li>" + movie.title + "</li>";
        })
        */
        }
            </div>
    );
    }


}

export default Movies;
