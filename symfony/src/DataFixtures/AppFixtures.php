<?php

namespace App\DataFixtures;

use App\Entity\Genre;
use App\Entity\Movie;
use App\Entity\Tag;
use App\Entity\Type;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{

    /** @var Generator */
    protected $faker;

    public function __construct(EntityManagerInterface $em) {
        $this->genreRepo = $em->getRepository(Genre::class);
        $this->tagRepo = $em->getRepository(Tag::class);
        $this->typeRepo = $em->getRepository(Type::class);
    }

    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();

        // Genre
        $genreCount = 5;
        for($i = 0; $i < $genreCount; $i++) {
            $genre = new Genre();
            $genre->setName($this->faker->text('12'));
            $genre->setSlug($this->faker->slug( '1'));
            $this->addReference('genre-'.$i, $genre);

            $manager->persist($genre);
        }

        // Tag
        $tagCount = 5;
        for($i = 0; $i < $tagCount; $i++) {
            $tag = new Tag();
            $tag->setName($this->faker->text('12'));
            $tag->setSlug($this->faker->slug( '1'));
            $this->addReference('tag-'.$i, $tag);

            $manager->persist($tag);
        }

        // Type

        $type = new Type();
        $type->setName('Movie');
        $type->setSlug(('movie'));

        $type2 = new Type();
        $type2->setName('TV Show');
        $type2->setSlug(('show'));

        $this->addReference('type-movie', $type);
        $this->addReference('type-show', $type2);

        $manager->persist($type);
        $manager->persist($type2);



        // User
        for($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setEmail($this->faker->email());
            $user->setNameFirst($this->faker->firstName);
            $user->setNameLast($this->faker->lastName);
            $user->setPassword($this->faker->password());
            $user->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'));

            $manager->persist($user);
        }

        // Movies
        for($i= 0; $i < 20; $i++) {
            $movie = new Movie();
            $movie->setTitle($this->faker->text('32'));
            $movie->setSlug($this->faker->slug());


            $type = $this->faker->randomElement(['movie', 'show']);
            $typeChoice = $this->typeRepo->findOneBy(['name' => $type]);


            $movie->setType($this->getReference('type-' . $this->faker->randomElement(['movie', 'show'])));
            $movie->setCreatedAt(new \DateTime('now'));


            // Add up to five genres
            $numGenres = rand(1, 5);
            for($x= 0; $x < $numGenres; $x++) {
                $movie->addGenre($this->getReference('genre-' . rand(0, $genreCount - 1)));
            }

            // Add up to five tags
            $numTags = rand(1, 3);
            for($y=0; $y < $numTags; $y++) {
                $movie->addTag($this->getReference('tag-' . rand(0, $tagCount - 1)));
            }

            $manager->persist($movie);
        }

        $manager->flush();
    }
}
