<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\GenreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GenreController
 * @package App\Controller
 */
class GenreContoller extends AbstractController
{


    /**
     * GenreContoller constructor.
     * @param GenreRepository $genreRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(GenreRepository $genreRepository, EntityManagerInterface $em)
    {
        $this->genreRepository = $genreRepository;
        $this->em = $em;
    }

    /**
     * @Route("/genres", name="genres", methods={"GET"})
     */
    public function genres() {
        // Get all movies
        $genres = $this->genreRepository->findAll();

        // Show count
        $data['count'] = count($genres);

        // Get movie details
        $data['genres'] = [];
        foreach($genres as $genre) {
            $data['genres'][] = $this->getGenreDetails($genre);
        }
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/genres/{id}", name="genres_single", methods={"GET"})
     */
    public function genre($id, Request $request) {
        // Get single movies
        $genre = $this->genreRepository->findOneBy(['id' => $id]);

        // Get genre details
        $data = $this->getGenreDetails($genre);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/genres", name="genres_add", methods={"POST"})
     */
    public function genreAdd(Request $request) {
        $name =  $request->request->get('name');

        if(empty($name)) {
            throw new NotFoundHttpException('Name is mandatory');
        }

        // Check for duplicate genres
        if($this->genreRepository->findOneBy(['name' => $name])) {
            return new Response('Add Failed. Duplicate genre: ' . $name . '.');
        }

        $genre = new Genre();
        $genre->setName($name);

        $this->em->persist($genre);
        $this->em->flush();

        return new Response('Added new genre: ' . $name);
    }

    /**
     * @Route("/genres/{id}", name="genres_update", methods={"PATCH"})
     */
    public function genreUpdate($id, Request $request) {
        $name =  $request->request->get('name');


        // Get single genre
        $genre = $this->genreRepository->findOneBy(['id' => $id]);

        if(!$genre) {
            return new Response('Genre not found with id: ' . $id);
        }

        // Get genre details
        if($name)
            $genre->setName($name);

        $this->em->persist($genre);
        $this->em->flush();

        return new Response('Updated genre: '. $name ." (id: " . $id . ")");
    }

    /**
     * @Route("/genres/{id}", name="genres_delete", methods={"DELETE"})
     */
    public function genreDelete($id, Request $request) {
        $genre = $this->genreRepository->findOneBy(['id' => $id]);
        if(!$genre) {
            return new Response('Could not find genre: '. $id);
        }
        $name = $genre->getName();
        $this->em->remove($genre);
        $this->em->flush();
        return new Response('Successfully deleted genre: ' . $name . " (id: " . $id . ")");
    }



    /**
     * @param $genre
     * @return array|void
     */
    public function getGenreDetails($genre) {
        if(!$genre) {
            throw new NotFoundHttpException('Genre not found.');
            return;
        }


        return [
            'id' => $genre->getId(),
            'name' => $genre->getName(),
        ];
    }
}
