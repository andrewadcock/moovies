<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TagController
 * @package App\Controller
 */
class TagController extends AbstractController
{


    /**
     * TagController constructor.
     * @param TagRepository $tagRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(TagRepository $tagRepository, EntityManagerInterface $em)
    {
        $this->tagRepository = $tagRepository;
        $this->em = $em;
    }

    /**
     * @Route("/tags", name="tags", methods={"GET"})
     */
    public function tags() {

        // Get all tags
        $tags = $this->tagRepository->findAll();

        // Show count
        $data['count'] = count($tags);

        // Get movie details
        $data['tags'] = [];
        foreach($tags as $tag) {
            $data['tags'][] = $this->getTagDetails($tag);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/tags/{id}", name="tags_single", methods={"GET"})
     */
    public function tag($id, Request $request) {
        // Get all movies
        $tag = $this->tagRepository->findOneBy(['id' => $id]);

        // Get tag details
        $data = $this->getTagDetails($tag);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/tags/add", name="tags_add", methods={"POST"})
     */
    public function tagAdd(Request $request) {
        $name =  $request->request->get('name');

        if(empty($name)) {
            throw new NotFoundHttpException('Name is mandatory');
        }

        // Check for duplicate tags
        if($this->tagRepository->findOneBy(['name' => $name])) {
            return new Response('Add Failed. Duplicate tag: ' . $name . '.');
        }

        $tag = new Tag();
        $tag->setName($name);

        $this->em->persist($tag);
        $this->em->flush();

        return new Response('Added new tag: ' . $name);
    }

    /**
     * @Route("/tags/{id}", name="tags_update", methods={"PATCH"})
     */
    public function tagUpdate($id, Request $request) {
        $name =  $request->request->get('name');


        // Get single tag
        $tag = $this->tagRepository->findOneBy(['id' => $id]);

        if(!$tag) {
            return new Response('Tag not found with id: ' . $id);
        }

        // Get genre details
        if($name)
            $tag->setName($name);

        $this->em->persist($tag);
        $this->em->flush();

        return new Response('Updated tag: '. $name ." (id: " . $id . ")");
    }

    /**
     * @Route("/tags/{id}", name="tags_delete", methods={"DELETE"})
     */
    public function tagDelete($id, Request $request) {
        $tag = $this->tagRepository->findOneBy(['id' => $id]);
        if(!$tag) {
            return new Response('Could not find tag: '. $id);
        }
        $name = $tag->getName();
        $this->em->remove($tag);
        $this->em->flush();
        return new Response('Successfully deleted tag: ' . $name . " (id: " . $id . ")");
    }

    /**
     * @param $tag
     * @return array
     */
    public function getTagDetails($tag) {
        if(!$tag) {
            throw new NotFoundHttpException('Tag not found.');
        }

        return [
            'id' => $tag->getId(),
            'name' => $tag->getName(),
        ];
    }
}
