<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Movie;
use App\Entity\Type;
use App\Repository\GenreRepository;
use App\Repository\MovieRepository;
use App\Repository\TagRepository;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Flex\PackageResolver;

/**
 * Class MovieController
 * @package App\Controller
 */
class MovieController extends AbstractController
{

    /**
     * MovieController constructor.
     * @param MovieRepository $movieRepository
     */
    public function __construct(MovieRepository $movieRepository, GenreRepository $genreRepository, TagRepository $tagRepository, TypeRepository $typeRepository, EntityManagerInterface $em)
    {
        $this->movieRepository = $movieRepository;
        $this->genreRepository = $genreRepository;
        $this->tagRepository = $tagRepository;
        $this->typeRepository = $typeRepository;
        $this->em = $em;
    }

    /**
     * @Route("/movies", name="movie", methods={"GET"})
     */
    public function movies()
    {
        // Get all movies
        $movies = $this->movieRepository->findAll();

        // Show count
        $data['count'] = count($movies);

        // Get movie details
        $data['movies'] = [];
        foreach($movies as $movie) {
            $data['movies'][] = $this->getMovieDetails($movie);
        }
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/movies/{id}", name="movie_single", methods={"GET"})
     */
    public function movie($id, Request $request) {

        // Get movie from db
        $movie = $this->movieRepository->findOneBy(['id' => $id]);

        // Get movie details
        $data = $this->getMovieDetails($movie);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/movies", name="movie_add", methods={"PUT"})
     */
    public function movieAdd(Request $request) {

        // Get request vars
        $title = $request->request->get('title');
        $genreIds = $request->request->get('genre');
        $tagIds = $request->request->get('tag');
        $typeName = $request->request->get('type');

        // If no title return error
        if(empty($title)) {
            throw new NotFoundHttpException('Title is mandatory');
        }

        // Check for duplicate titles
        if($this->movieRepository->findOneBy(['title' => $title])) {
            return new Response('Add Failed. Duplicate movie title: ' . $title . '.');
        }

        $genres = $this->getGenres($genreIds);
        $tags = $this->getTags($tagIds);
        $this->confirmType($typeName);

        $movie = new Movie();
        $this->setMovieDetails($movie, $genres, $tags, $title, $typeName);

        $this->em->persist($movie);
        $this->em->flush();

        return new Response('Added new ' . $typeName . ': ' . $title);

    }

    /**
     * @Route("/movies/{id}", name="movie_delete", methods={"DELETE"})
     */
    public function movieDelete($id, Request $request) {
        $movie = $this->movieRepository->findOneBy(['id' => $id]);
        $title = $movie->getTitle();
        $this->em->remove($movie);
        $this->em->flush();
        return new Response($title. ' has been deleted.');

    }

    /**
     * @Route("/movies/{id}", name="movie_update", methods={"PATCH"})
     */
    public function movieUpdate($id, Request $request) {

        // Get movie by id
        $movie = $this->movieRepository->findOneBy(['id' => $id]);

        if(!$movie) {
            return new NotFoundHttpException('Movie with id ' . $id . ' not found');
        }

        // Get items from request
        $title = $request->request->get('title');
        $genreIds = $request->request->get('genre');
        $tagIds = $request->request->get('tag');
        $typeName = $request->request->get('type');

        $genres = $this->getGenres($genreIds);
        $tags = $this->getTags($tagIds);
        $this->confirmType($typeName);

        if($this->movieRepository->findOneBy(['title' => $title])) {
            return new Response('Title already exists: ' . $title);
        }

        $this->setMovieDetails($movie, $genres, $tags, $title, $typeName);

        $this->em->persist($movie);
        $this->em->flush();

        return new Response('Updated ' . $typeName . ": ". $title);
    }

    /**
     * @param $movie
     * @return mixed
     */
    public function getMovieDetails(Movie $movie) {
        if(!$movie) {
            throw new NotFoundHttpException('Movie not found.');
            return;
        }
        $genres = [];
        foreach($movie->getGenre() as $genre ) {
            $genres[] = $genre->getName();
        }


        $tags = [];
        foreach($movie->getTags() as $tag) {
            $tags[] = $tag->getName();
        }

        $data = [
            'id' => $movie->getId(),
            'title' => $movie->getTitle(),
            'genre' => $genres,
            'tags' => $tags,
            'createdAt' => $movie->getCreatedAt()
        ];
        return $data;
    }

    /**
     * @param $genreIds
     * @return array
     */
    public function getGenres($genreIds) {
        if($genreIds) {

            $genres = [];
            foreach($genreIds as $genreSingle) {
                $genres[] = $this->genreRepository->findByID($genreSingle);
            }

            foreach($genres as $genre) {
                if(empty($genre)) {
                    throw new NotFoundHttpException('One or more Genres not found. Check /genres for available genres.');
                }
            }

            return $genres;
        }
    }

    /**
     * @param $tagIds
     * @return bool
     */
    public function getTags($tagIds) {

        if($tagIds) {

            foreach($tagIds as $tagSingle) {
                $tags[] = $this->tagRepository->findByID($tagSingle);
            }

            foreach($tags as $tag) {
                if (empty($tag)) {
                    throw new NotFoundHttpException('One or more Tags not found. Check /tags for available tags.');
                }
            }

            return $tags;
        }
    }

    /**
     * @param $typeName
     */
    public function confirmType($typeName) {
        if($typeName) {

            $type = $this->typeRepository->findOneBy(['name' => $typeName]);

            if(empty($type)) {
                throw new NotFoundHttpException('Type not found. Options = {movie, show}');
            } else {
                return $type;
            }
        }

    }

    /**
     * @param $movie
     * @param $genres
     * @param $tags
     * @param $title
     * @param null $typeName
     * @return mixed
     */
    public function setMovieDetails($movie, $genres, $tags, $title, $typeName = null){
        $movie->setTitle($title);

        if($genres) {

            if(!array($genres)) {
                $movie->addGenre($genres);
            } else {
                foreach($genres as $genreSingle) {

                    $movie->addGenre($this->genreRepository->findOneBy(['id' => $genreSingle[0]->getId()]));
                }
            }
        }

        if(!empty($tags)) {
            if(!array($tags)) {
                $movie->addTag($tags);
            } else {

                foreach($tags as $tagSingle) {
                    $movie->addTag($this->tagRepository->findOneBy(['id' => $tagSingle[0]->getId()]));
                }
            }
        }

        // Get type entity
        $movieType = $this->typeRepository->findOneBy(['name' => $typeName]);
        if(!$movieType) {
            $movieType = $this->typeRepository->findOneBy(['name' => 'movie']);
        }
        $movie->setType($movieType);
        $movie->setCreatedAt(new \DateTime());

        return $movie;
    }
}
