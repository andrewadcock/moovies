<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Type;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TypeController
 * @package App\Controller
 */
class TypeController extends AbstractController
{


    /**
     * TypeController constructor.
     * @param TypeRepository $typeRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(TypeRepository $typeRepository, EntityManagerInterface $em)
    {
        $this->typeRepository = $typeRepository;
        $this->em = $em;
    }

    /**
     * @Route("/types", name="types", methods={"GET"})
     */
    public function types() {

        // Get all types
        $types = $this->typeRepository->findAll();

        // Show count
        $data['count'] = count($types);

        // Get movie details
        $data['types'] = [];
        foreach($types as $type) {
            $data['types'][] = $this->getTypeDetails($type);
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/types/{id}", name="types_single", methods={"GET"})
     */
    public function type($id, Request $request) {
        // Get all movies
        $type = $this->typeRepository->findOneBy(['id' => $id]);

        // Get genre details
        $data = $this->getTypeDetails($type);

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/types/{id}", name="types_update", methods={"PATCH"})
     */
    public function typeUpdate($id, Request $request) {
        $name =  $request->request->get('name');

        // Get single tag
        $type = $this->typeRepository->findOneBy(['id' => $id]);

        if(!$type) {
            return new Response('Tag not found with id: ' . $id);
        }

        // Get genre details
        if($name)
            $type->setName($name);

        $this->em->persist($type);
        $this->em->flush();

        return new Response('Updated tag: '. $name ." (id: " . $id . ")");
    }

    /**
     * @Route("/types", name="types_add", methods={"POST"})
     */
    public function typesAdd(Request $request) {
        $name =  $request->request->get('name');

        if(empty($name)) {
            throw new NotFoundHttpException('Name is mandatory');
        }

        // Check for duplicate types
        if($this->typeRepository->findOneBy(['name' => $name])) {
            return new Response('Add Failed. Duplicate type: ' . $name . '.');
        }

        $type = new Type();
        $type->setName($name);

        $this->em->persist($type);
        $this->em->flush();

        return new Response('Added new type: ' . $name);
    }

    /**
     * @Route("/types/{id}", name="types_delete", methods={"DELETE"})
     */
    public function typesDelete($id, Request $request) {
        $type = $this->typeRepository->findOneBy(['id' => $id]);
        if(!$type) {
            return new Response('Could not find type: '. $id);
        }
        $name = $type->getName();
        $this->em->remove($type);
        $this->em->flush();
        return new Response($name. ' has been deleted.');
    }

    /**
     * @param $type
     * @return array
     */
    public function getTypeDetails($type) {
        if(!$type) {
            throw new NotFoundHttpException('Type not found.');
        }

        return [
            'id' => $type->getId(),
            'name' => $type->getName(),
        ];
    }


}
