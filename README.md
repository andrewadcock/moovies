# Docker for Symfony and React

## Description
A simple tool for keeping track of and selecting movies/tv shows to watch.

## Usage
1. Clone the repository
    1. `git clone https://gitlab.com/andrewadcock/moovies`
2. CD in to the directory
    1. `cd moovies`
3. Create .env file in the symfony directory
    1. `touch symfony/.env` 
4. Run `docker-compose up`
5. (Optional) add entries to `/etc/hosts`
    1. ```
       172.28.0.6 moovies.local www.moovies.local
       127.0.0.1 moovies.local www.moovies.local
       ```
    2. Visit sites at http://react.local:3000 and http://symfony.local:8000
6. (Optional) Login to Adminer
    1. Navigate to `http://127.0.0.1:8080`
    2. Login to the, already created, database
        1. ```
             System: MySQL
             Server: mysql
           Username: symfony
           Password: symfony
           Database: symfony
         
## Issues?
Sometimes `composer install` fails. If you get a warning like: `Warning: require(/var/www/symfony/vendor/autoload.php): failed to open stream: No such file or directory in /var/www/symfony/config/bootstrap.php on line 5` you'll need to run `composer install` manually.

1. If composer is NOT installed on startup: 
   1. `docker exec -it movie-list_symfony_1 "bash"`
   2. `composer install`

## Endpoints
#### Movies
  - All movies (GET): http://localhost:8000/movies 
  - Single movie (GET): http://localhost:8000/movies/{id}
  - Add movie (PUT): http://localhost:8000/movies 
    - title: string
    - type: string
    - genre: array
    - tag: array
  - Update movie (PATCH): http://localhost:8000/movies/{id}
    - title: string
    - type: string
    - genre: array
    - tag: array
  - Delete movie (DELETE): http://localhost:8000/movies/{id}

#### Types
   - All types (GET): https://localhost:8000/types
   - Single type (GET): https://localhost:8000/types/{id}
   - Add types (PUT): https://localhost:8000/types/add
      - name: string
   - Update type (PATCH): https://localhost:8000/genres/{id}
      - name: string
   - Delete type (DELETE): https://localhost:8000/types/{id}

#### Genres
   - All genres (GET): https://localhost:8000/genres
   - Single genre (GET): https://localhost:8000/genres/{id}
   - Add genre (PUT): https://localhost:8000/genres
         - name: string
   - Update genre (PATCH): https://localhost:8000/genres/{id}
         - name: string
   - Delete genre (DELETE): https://localhost:8000/genres/{id}

#### Tags
   - All tags (GET): https://localhost:8000/tags
   - Single tag (GET): https://localhost:8000/tags/{id}
   - Add tag (PUT): https://localhost:8000/tags
      - name: string
   - Update tag (PATCH): https://localhost:8000/genres/{id}
      - name: string
   - Delete tag (DELETE): https://localhost:8000/tags/{id}

